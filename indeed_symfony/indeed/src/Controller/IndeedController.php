<?php

namespace App\Controller;

use App\Entity\Offres;
use App\Repository\ContratsRepository;
use App\Repository\OffresRepository;
use App\Repository\TypeContratsRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\DependencyInjection\Security\UserProvider\EntityFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndeedController extends AbstractController
{
    /**
     * @Route("/", name="offre")
     */
    public function index(OffresRepository $repository): Response
    {
        $offres = $repository->findAll();

        return $this->render('indeed/index.html.twig', [
            'offres' => $offres
        ]);
    }

    /**
     * @Route("/form",name="offre.form")
     */
    public function form(Request $request, EntityManagerInterface $manager_interface, ContratsRepository $contrat, TypeContratsRepository $type_contrat): Response
    {
        $offre = new Offres;
        $cdd = $contrat->findOneBy(["titre" => "CDD"]);
        $cdi = $contrat->findOneBy(["titre" => "CDI"]);
        $freelance = $contrat->findOneBy(["titre" => "Freelance"]);
        $tempsPlein = $type_contrat->findOneBy(["titre" => "Temps Plein"]);
        $tempsPartiel = $type_contrat->findOneBy(["titre" => "Temps Partiel"]);
        $form = $this->createFormBuilder($offre)
            ->add("Title", TextType::class)
            ->add("Description", TextType::class)
            ->add("Adresse", TextType::class)
            ->add("Code_postal", TextType::class)
            ->add("Ville", TextType::class)
            ->add("Fin_mission", DateType::class)
            ->add("key_contrats", ChoiceType::class, [
                'choices' => [
                    'CDI' => $cdd,
                    'CDD' => $cdi,
                    'Freelance' => $freelance,
                ],
            ])

            ->add("key_type_contrats", ChoiceType::class, [
                'choices' => [
                    'Temps plein' => $tempsPlein,
                    'Temps partiel' => $tempsPartiel,
                ],
            ], ['label' => "Choix du type de contrat"],)
            ->add("Envoyer", SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary',
                    'role' => 'button',
                ]
            ])
            ->getForm();
        if (empty($offre->getDateCreation())) {
            $offre->setDateCreation(new \DateTime('now'));
        } else {
            $offre->setDateMaj(new \DateTime('now'));
        }
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $manager_interface->persist($offre);
            $manager_interface->flush();
        }
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirect('/');
        }
        return $this->render('indeed/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/offres/{id}",name="offre.show")
     */
    public function show(Offres $offre)
    {
        return $this->render('indeed/offre.html.twig', [
            'offre' => $offre
        ]);
    }

    /**
     * @Route("/gestion", name="offre.gestion")
     */
    public function gestion(OffresRepository $repository): Response
    {
        $offres = $repository->findAll();

        return $this->render('indeed/gestion.html.twig', [
            'offres' => $offres
        ]);
    }

    /**
     * @Route("/gestion/{id}",name="offre.crud")
     */
    public function crud(Offres $offre, Request $request, EntityManagerInterface $manager_interface, ContratsRepository $contrat, TypeContratsRepository $type_contrat)
    {
        $cdd = $contrat->findOneBy(["titre" => "CDD"]);
        $cdi = $contrat->findOneBy(["titre" => "CDI"]);
        $freelance = $contrat->findOneBy(["titre" => "Freelance"]);
        $tempsPlein = $type_contrat->findOneBy(["titre" => "Temps Plein"]);
        $tempsPartiel = $type_contrat->findOneBy(["titre" => "Temps Partiel"]);
        $form = $this->createFormBuilder($offre)
            ->add("Title", TextType::class)
            ->add("Description", TextType::class)
            ->add("Adresse", TextType::class)
            ->add("Code_postal", TextType::class)
            ->add("Ville", TextType::class)
            ->add("Fin_mission", DateType::class)
            ->add("key_contrats", ChoiceType::class, [
                'choices' => [
                    'CDI' => $cdd,
                    'CDD' => $cdi,
                    'Freelance' => $freelance,
                ],
            ])

            ->add("key_type_contrats", ChoiceType::class, [
                'choices' => [
                    'Temps plein' => $tempsPlein,
                    'Temps partiel' => $tempsPartiel,
                ],
            ], ['label' => "Choix du type de contrat"],)
            ->add("Envoyer", SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary',
                    'role' => 'button',
                ]
            ])
            ->getForm();
        if (empty($offre->getDateCreation())) {
            $offre->setDateCreation(new \DateTime('now'));
        } else {
            $offre->setDateMaj(new \DateTime('now'));
        }
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $manager_interface->persist($offre);
            $manager_interface->flush();
        }
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirect('/');
        }
        return $this->render('indeed/crud.html.twig', [
            'form' => $form->createView(),
            'offre' => $offre
        ]);
    }


    /**
     * @Route("/delete/{id}", name="offre.delete")
     */
    public function delete(Offres $offre, EntityManagerInterface $manager_interface): Response
    {
        $manager_interface->remove($offre);
        $manager_interface->flush();
        return $this->redirect('/gestion');
    }
}
