<?php

namespace App\DataFixtures;

use App\Entity\Contrats;
use App\Entity\Offres;
use App\Entity\TypeContrats;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $cdd = new Contrats();
        $cdd->setTitre("CDD");
        $manager->persist($cdd);
        $cdi = new Contrats();
        $cdi->setTitre("CDI");
        $manager->persist($cdi);
        $freelance = new Contrats();
        $freelance->setTitre("Freelance");
        $manager->persist($freelance);
        $temps_plein = new TypeContrats();
        $temps_plein->setTitre("Temps Plein");
        $manager->persist($temps_plein);
        $temps_partiel = new TypeContrats();
        $temps_partiel->setTitre("Temps Partiel");
        $manager->persist($temps_partiel);

        for ($i = 0; $i < 20; $i++) {
            $contrat = $faker->randomElement(array($cdd, $cdi, $freelance));
            $type_contrat = $faker->randomElement(array($temps_plein, $temps_partiel));
            $offres = new Offres();
            $offres->setTitle($faker->sentence(2, true))
                ->setDescription($faker->sentence(10, true))
                ->setAdresse($faker->sentence(10, true))
                ->setCodePostal($faker->postcode())
                ->setVille($faker->city())
                ->setDateCreation($faker->dateTimeBetween('-6 months', '-3 months'))
                ->setDateMaj($faker->dateTimeBetween('-2 months', 'now'))
                ->setKeyContrats($contrat)
                ->setKeyTypeContrats($type_contrat);
            if ($contrat->getTitre() == "CDD" or $contrat->getTitre() == "Freelance") {
                $offres->setFinMission($faker->dateTimeBetween('+1 months', '+2 months'));
            } else {
                $offres->setFinMission(Null);
            }

            $manager->persist($offres);
        }

        $manager->flush();
    }
}
