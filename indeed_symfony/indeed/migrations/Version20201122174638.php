<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201122174638 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE contrats (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE offres (id INT AUTO_INCREMENT NOT NULL, key_contrats_id INT NOT NULL, key_type_contrats_id INT NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, adresse VARCHAR(255) NOT NULL, ville VARCHAR(255) NOT NULL, date_creation DATE NOT NULL, date_maj DATE DEFAULT NULL, code_postal VARCHAR(255) NOT NULL, fin_mission DATE DEFAULT NULL, INDEX IDX_C6AC3544310F9095 (key_contrats_id), INDEX IDX_C6AC35443798AB85 (key_type_contrats_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_contrats (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE offres ADD CONSTRAINT FK_C6AC3544310F9095 FOREIGN KEY (key_contrats_id) REFERENCES contrats (id)');
        $this->addSql('ALTER TABLE offres ADD CONSTRAINT FK_C6AC35443798AB85 FOREIGN KEY (key_type_contrats_id) REFERENCES type_contrats (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE offres DROP FOREIGN KEY FK_C6AC3544310F9095');
        $this->addSql('ALTER TABLE offres DROP FOREIGN KEY FK_C6AC35443798AB85');
        $this->addSql('DROP TABLE contrats');
        $this->addSql('DROP TABLE offres');
        $this->addSql('DROP TABLE type_contrats');
    }
}
